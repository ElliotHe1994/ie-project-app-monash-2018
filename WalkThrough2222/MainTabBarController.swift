//
//  MainTabBarController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 8/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import Photos
import RevealingSplashView

/// Main tab bar controller controls the tab bar items
class MainTabBarController: UITabBarController, UITabBarControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let index = viewControllers?.index(of: viewController)
        if index == 2 {
            let alertCT = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertCT.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
                let cameraController = CameraController()
                self.present(cameraController, animated: true, completion: nil)
            }))
            alertCT.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
                
                let layout = UICollectionViewFlowLayout()
                let choosePictureController = ChoosePictureController(collectionViewLayout: layout)
                let navController = UINavigationController(rootViewController: choosePictureController)
                self.present(navController, animated: true, completion: nil)
            }))
            alertCT.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertCT.popoverPresentationController?.sourceView = tabBar
            present(alertCT, animated: true, completion: nil)
            
            return false
        }
        return true
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.delegate = self
        
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "logowhite")!, iconInitialSize: CGSize(width:180, height:180), backgroundImage: UIImage(named: "background")!)
        
        revealingSplashView.useCustomIconColor = false
        revealingSplashView.iconColor = UIColor.white
        revealingSplashView.animationType = SplashAnimationType.popAndZoomOut
        
//        let window = UIApplication.sharedApplication().keyWindow
//        window?.addSubview(revealingSplashView)
        self.view.addSubview(revealingSplashView)
        
        revealingSplashView.startAnimation() {
            if Firebase.Auth.auth().currentUser == nil || !(Firebase.Auth.auth().currentUser?.isEmailVerified)! {
                DispatchQueue.main.async {
                    let loginController = LoginController()
                    let navController = UINavigationController(rootViewController: loginController)
                    self.present(navController, animated: true, completion: nil)
                }
                return
            }
        }
        
        initView()
        
    }
    
    func initView() {
        
        let dictionaryNavController = makeNavController(rootViewController: DictionaryViewController(), unselected: #imageLiteral(resourceName: "help"), selected: #imageLiteral(resourceName: "help_selected"))
        
        let websiteNavController = makeNavController(rootViewController: WebsiteController(), unselected: #imageLiteral(resourceName: "Homepage"), selected: #imageLiteral(resourceName: "Homepage_selected"))
        
        let cameraNavController = makeNavController(unselected: #imageLiteral(resourceName: "camera").withRenderingMode(.alwaysOriginal), selected: #imageLiteral(resourceName: "camera").withRenderingMode(.alwaysOriginal))
        
        let mapNavController = makeNavController(rootViewController: CommunityMapViewController(), unselected: #imageLiteral(resourceName: "Positioning"), selected: #imageLiteral(resourceName: "Positioning_selected"))
        
        let profileNavController = makeNavController(rootViewController: UserBlogController(collectionViewLayout: UICollectionViewFlowLayout()), unselected: #imageLiteral(resourceName: "member"), selected: #imageLiteral(resourceName: "member_selected"))
            
//        tabBar.tintColor = .black
//        tabBar.barTintColor = UIColor.orange
        viewControllers = [websiteNavController, mapNavController, cameraNavController, dictionaryNavController, profileNavController]
        
        guard let barItems = tabBar.items else {return}
        for item in barItems {
            item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        }
    }
    
    private func makeNavController(rootViewController: UIViewController = UIViewController(), unselected: UIImage, selected: UIImage) -> UINavigationController {
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        viewController.tabBarItem.image = unselected
        viewController.tabBarItem.selectedImage = selected
        navController.navigationBar.applyNavigationGradient(colors: [UIColor.rgb(red: 54, green: 0, blue: 51) , UIColor.rgb(red: 11, green: 135, blue: 147)])
        navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        return navController
    }
}

extension UITabBar {
    override open var traitCollection: UITraitCollection {
        guard UIDevice.current.userInterfaceIdiom == .pad else {
            return super.traitCollection
        }
        
        return UITraitCollection(horizontalSizeClass: .compact)
    }
}

