//
//  Species.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 8/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

/// Dictionary controller class provides the table of the species and display the density map when users click a species
class DictionaryViewController: UITableViewController, UISearchBarDelegate {
    
    let SPECIES_ID_LIST = ["15119","8168","8169","8173","14290","14333","14969","8438","8200","14377","8154","8439","8158","7329","8451","8207","8442","8212","15954","8418","8419","8420","8422","8421","15751","5765","8350","8162","8436","8231","8402","11509","8424","8262","8450","8416","8175","8383"]
    let SPECIES_URL = "https://collections.museumvictoria.com.au/api/species/"
    
    lazy var searchBar: UISearchBar = {
        let sbar = UISearchBar()
        sbar.placeholder = "Enter species name"
        sbar.delegate = self
        return sbar
    }()
    
    
    /// filter the list when user type keywords
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchSpeciesList = self.speciesList
        } else {
            self.searchSpeciesList = self.speciesList.filter({ (specie) -> Bool in
                return specie.name.lowercased().contains(searchText.lowercased()) || specie.region.lowercased().contains(searchText.lowercased())
            })
        }
        self.tableView.reloadData()
    }
    
    /// show the cancel button when the search bar is clicked
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.white
        searchBar.showsCancelButton = true
    }
    
    /// dismiss the keyboard when the cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
    
    var speciesList = [Species]()
    var searchSpeciesList = [Species]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.addSubview(searchBar)
        let navBar = navigationController?.navigationBar
        searchBar.anchor(navBar?.topAnchor, left: navBar?.leftAnchor, bottom: navBar?.bottomAnchor, right: navBar?.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        navigationItem.title = "Species Density"
        initialSpecies(url: SPECIES_URL)
        tableView.register(DictionaryCell.self, forCellReuseIdentifier: "cellId")
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    /// hide the status bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /**
     * Function name: handleRefresh
     * Function description: refresh the whole table view
     */
    @objc private func handleRefresh() {
        self.speciesList.removeAll()
        self.searchSpeciesList.removeAll()
        initialSpecies(url: SPECIES_URL)
    }
    
    /// show the density map when user click one species
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let webvc = DetailController()
        let webvc = SpecieDescriptionController()
        webvc.specie = searchSpeciesList[indexPath[1]]
        webvc.speciesLabel.text = "\(searchSpeciesList[indexPath[1]].name)"
        present(webvc, animated: true, completion: nil)
        
    }
    
    /// dequeue the cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! DictionaryCell
        let specie = searchSpeciesList[indexPath.row]
        cell.specie = specie
        return cell
    }
    
    /// define the numbers of rows
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchSpeciesList.count
    }
    
    /// define the height of each row
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    /**
     * Function name: initialSpecies
     * Function description: initialize the species meta data
     */
    func initialSpecies(url: String) {
        self.speciesList = []
        for i in SPECIES_ID_LIST {
            Alamofire.request(url+i, method: .get).responseJSON { (response) in
                if response.result.isSuccess {
                    print("Success! Got the species data")
                    self.tableView?.refreshControl?.endRefreshing()
                    guard let speciesJSON: JSON = JSON(response.result.value) else {return}
                    do {
                        DispatchQueue.main.async {
                            
                                //                        print(result)
                            let q = speciesJSON["displayTitle"].stringValue.replacingOccurrences(of: "<em>", with: "", options: NSString.CompareOptions.literal, range: nil)
                            let p = q.replacingOccurrences(of: "</em>", with: "", options: NSString.CompareOptions.literal, range: nil)
                            let index = p.index(of:"(") ?? p.index(of:",") ?? p.endIndex
                            let beginning = p[..<index]
                            let name = String(beginning)
                            let region = speciesJSON["animalType"].stringValue
                            let imageURL = speciesJSON["media"][0]["small"]["uri"].stringValue
                            let densityURL = "https://biocache.ala.org.au/ws/geojson/radius-points?q=\(name)"
                            guard let url = URL(string: imageURL) else {
                                return
                            }
                            if let data = try? Data(contentsOf: url) {
                                guard let image: UIImage = UIImage(data: data) else {
                                    return
                                }
                                let tmp = Species(name: name, region: region, image: image, density: densityURL)
                                self.speciesList.append(tmp)
                                self.searchSpeciesList = self.speciesList
                                self.tableView.reloadData()
                            }
                        }
                        
                    } catch let jsonError {
                        print("Failed to parse JSON property: \(jsonError)")
                    }
                    
                }
                else {
                    print("Error \(response.result.error)")
                }
                
            }
        }
    }
    
}
