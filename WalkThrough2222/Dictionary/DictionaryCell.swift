//
//  DictionaryCell.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 1/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

/// Dictionary cell is a custom cell
class DictionaryCell: UITableViewCell {
    
    var specie: Species? {
        didSet {
            specieImage.image = specie?.image
            speciesNameLabel.text = specie?.name
            regionLabel.text = specie?.region
        }
    }
    
    private let specieImage : UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    private let speciesNameLabel: UILabel = {
        let label = UILabel()
        label.text = "sss"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    private let regionLabel: UILabel = {
        let label = UILabel()
        label.text = "region"
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(specieImage)
        _ = specieImage.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        addSubview(speciesNameLabel)
        _ = speciesNameLabel.anchor(topAnchor, left: specieImage.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 23, leftConstant: 12, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 24)
        
        addSubview(regionLabel)
        _ = regionLabel.anchor(speciesNameLabel.bottomAnchor, left: specieImage.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 12, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 24)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
