//
//  SpecieDescriptionController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 1/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

/// Specie Description controller provides the density map for users
class SpecieDescriptionController: UIViewController, GMSMapViewDelegate {
    
    var specie: Species? {
        didSet {
            name = (specie?.name)!
            JSONurl = (specie?.density)!
        }
    }
    
    var heatmapLayer: GMUHeatmapTileLayer!
    
//    let locationManager = CLLocationManager()
//    var currentLocation: CLLocationCoordinate2D?
    
    var densityMap: GMSMapView!
    var gradientColors = [UIColor.green, UIColor.red]
    var gradientStartPoints = [0.2, 1.0] as? [NSNumber]
    
    var name = ""
    var JSONurl = ""
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "goback"), for: .normal)
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()
    
    let legendView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "legend").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let speciesLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.textAlignment = .center
        label.numberOfLines = 0
//        label.frame = CGRect(x: 0, y: 0, width: 130, height: 80)
        return label
    }()
    
    let highLabel: UILabel = {
        let label = UILabel()
        label.text = "High"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 12)
        return label
    }()
    
    let lowLabel: UILabel = {
        let label = UILabel()
        label.text = "Low"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 12)
        return label
    }()
    
    @objc private func handleBack() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        densityMap = GMSMapView()
        densityMap.frame = CGRect.zero
        let camera = GMSCameraPosition.camera(withLatitude: -0.7893, longitude: 113.9213, zoom: 3.7)
        densityMap.camera = camera
        view = densityMap
        //        initLocationManager()
        
        densityMap.settings.myLocationButton = true
        densityMap.isMyLocationEnabled = true
        densityMap.settings.compassButton = true
        densityMap.settings.indoorPicker = false
        initButton()
        
        
        initMap()
        view.addSubview(speciesLabel)
        _ = speciesLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
//        heatmapLayer.map = densityMap
        
        view.addSubview(legendView)
        _ = legendView.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 50, rightConstant: 0, widthConstant: 20, heightConstant: 150)
        
        view.addSubview(highLabel)
        _ = highLabel.anchor(legendView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 11, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 15)
        
        view.addSubview(lowLabel)
        _ = lowLabel.anchor(nil, left: view.leftAnchor, bottom: legendView.topAnchor, right: nil, topConstant: 0, leftConstant: 12, bottomConstant: 8, rightConstant: 0, widthConstant: 30, heightConstant: 15)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /**
     * Function name: initMap
     * Function description: initialize the Google map
     */
    private func initMap() {
        heatmapLayer = GMUHeatmapTileLayer()
        heatmapLayer.radius = 80
        heatmapLayer.opacity = 0.8
        heatmapLayer.gradient = GMUGradient(colors: gradientColors, startPoints: gradientStartPoints!, colorMapSize: 256)
        initialMarkers(url: JSONurl)
    }
    
    /**
     * Function name: initialMarkers
     * Function description: initialize the markers on the map
     */
    func initialMarkers(url: String) {
        var list = [GMUWeightedLatLng]()
        let newURL = url.replacingOccurrences(of: " ", with: "%20", options: NSString.CompareOptions.literal, range: nil)
        DispatchQueue.main.async {
            Alamofire.request(newURL, method: .get).responseJSON { (response) in
                if response.result.isSuccess {
                    print("Success! Got the markers data")
                    
                    guard let markersJSON: JSON = JSON(response.data) else {return}
                    //                print(markersJSON)
                    //                let array = markersJSON.arrayValue
                    let markersArray = markersJSON["features"]
                    //                print(markersArray)
                    list = []
                    do {
                        for result in markersArray.arrayValue {
                            guard let long = result["geometry"]["coordinates"][0].double else {continue}
                            guard let lat = result["geometry"]["coordinates"][1].double else {continue}
                            print("lat: \(lat), long: \(long)")
//                            let wildlifeName = self.name
                            let coords = GMUWeightedLatLng(coordinate: CLLocationCoordinate2DMake(lat as! CLLocationDegrees, long as! CLLocationDegrees), intensity: 1.0)
                            list.append(coords)
                        }
                        print("list is: \(list)")
                        self.heatmapLayer.weightedData = list
                        self.heatmapLayer.map = self.densityMap
                    } catch let jsonError {
                        print("Failed to parse JSON property: \(jsonError)")
                    }
                    
                }
                else {
                    print("Error \(response.result.error)")
                }
            }
        }
    }
    
    func initButton() {
        view.addSubview(backButton)
        _ = backButton.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
    }

    
}
