//
//  CustomIconView.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 5/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

class CustomIconView: UIView {
    
    var urlEnd: String?
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    func loadImage(url: String) {
        urlEnd = url
        guard let url = URL(string: url) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let ex = error {
                print("Failed to fetch post image: \(ex)")
                return
            }
            if url.absoluteString != self.urlEnd {
                return
            }
            guard let imageData = data else {return}
            let photoImage = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.imageView.image = photoImage?.withRenderingMode(.alwaysOriginal)
            }
            }.resume()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        _ = imageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.layer.cornerRadius = 30 / 2
        imageView.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
