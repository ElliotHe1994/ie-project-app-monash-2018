//
//  CustomImageView.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 23/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

var imageCache = [String: UIImage]()

class CustomImageView: UIImageView {
    
    var urlEnd: String?
    
    func loadImage(url: String) {
        urlEnd = url
        if let cachedImage = imageCache[urlEnd!] {
            self.image = cachedImage
            return
        }
        guard let url = URL(string: url) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let ex = error {
                print("Failed to fetch post image: \(ex)")
                return
            }
            if url.absoluteString != self.urlEnd {
                return
            }
            guard let imageData = data else {return}
            let photoImage = UIImage(data: imageData)
            
            imageCache[url.absoluteString] = photoImage
            
            DispatchQueue.main.async {
                self.image = photoImage
            }
        }.resume()
        
    }
    
}
