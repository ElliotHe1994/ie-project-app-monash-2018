//
//  POIItem.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 4/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation

class POIItem: NSObject, GMUClusterItem {
    
    var position: CLLocationCoordinate2D
    
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
    
}
