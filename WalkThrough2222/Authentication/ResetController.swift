//
//  ResetController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 10/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

/// Reset controller provides the function for letting users to find password
class ResetController: UIViewController {
    
    let emailTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "email address"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let findPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Find Password", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleReset), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 255, green: 59, blue: 48)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
    }()
    
    /**
     * Function name: changeButtonColor
     * Function description: change the button color when the input field is full filled
     */
    @objc private func changeButtonColor() {
        let isFieldFullFilled = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count ?? 0 > 0
        
        if isFieldFullFilled {
            findPasswordButton.isEnabled = true
            findPasswordButton.backgroundColor = UIColor.rgb(red: 166, green: 77, blue: 255)
        } else {
            findPasswordButton.isEnabled = false
            findPasswordButton.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        }
    }
    
    /**
     * Function name: handleReset
     * Function description: handle reset password action after user type the email address
     */
    @objc private func handleReset() {
        guard let emailAddress = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
        SVProgressHUD.show(withStatus: "I'm working on it :)")
        UIApplication.shared.beginIgnoringInteractionEvents()
        Auth.auth().sendPasswordReset(withEmail: emailAddress) { (error) in
            if let ex = error {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "The email address does not exist in the system")
                SVProgressHUD.dismiss(withDelay: 1)
            } else {
                SVProgressHUD.dismiss()
                SVProgressHUD.showSuccess(withStatus: "The reset email has been sent to your email")
                SVProgressHUD.dismiss(withDelay: 1.5)
                self.dismiss(animated: true, completion: nil)
            }
        }
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
    /**
     * Function name: handleCancel
     * Function description: dismiss the page after user hit cancel button
     */
    @objc private func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        initialLayout()
        
    }
    
    /**
     * Function name: dismissKeyboard
     * Function description: dismiss the keyboard
     */
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func initialLayout() {
        view.addSubview(emailTF)
        _ = emailTF.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 100, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 50)
        view.addSubview(findPasswordButton)
        _ = findPasswordButton.anchor(emailTF.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 12, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 40)
        view.addSubview(cancelButton)
        _ = cancelButton.anchor(findPasswordButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 12, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 40)
    }
    
    
}
