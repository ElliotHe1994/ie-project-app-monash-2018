//
//  LoginController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 10/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

// Login controller class for providing users to login to the system
class LoginController: UIViewController {
    
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("No Account?", for: .normal)
        button.addTarget(self, action: #selector(moveToRegister), for: .touchUpInside)
        return button
    }()
    
    let aboutButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("About", for: .normal)
        button.addTarget(self, action: #selector(moveToAbout), for: .touchUpInside)
        return button
    }()
    
    let findPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Forgot Password?", for: .normal)
        button.addTarget(self, action: #selector(moveToReset), for: .touchUpInside)
        return button
    }()
    
    let emailTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "email address"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let pwdTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "password"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.isSecureTextEntry = true
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    let logoContainer: UIView = {
        let view = UIView()
        let logoImage = UIImageView(image: #imageLiteral(resourceName: "Logo-1"))
        logoImage.contentMode = .scaleAspectFill
        view.addSubview(logoImage)
        _ = logoImage.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 92)
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        view.backgroundColor = UIColor.rgb(red: 166, green: 77, blue: 255)
        return view
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // dismiss the keyboard when users tap other places of the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        view.addSubview(logoContainer)
        logoContainer.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        perform(#selector(showWalkController), with: nil, afterDelay: 0)

        view.addSubview(registerButton)
        _ = registerButton.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        initialInputFields()
        
        view.addSubview(aboutButton)
        _ = aboutButton.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    /**
     * Function name: dismissKeyboard
     * Function description: dismiss the keyboard
     */
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /**
     * Function name: moveToRegister
     * Function description: move to the register page
     */
    @objc func moveToRegister() {
        
        let registrationCT = RegistrationController()
        navigationController?.pushViewController(registrationCT, animated: true)
        
    }
    
    /**
     * Function name: moveToAbout
     * Function description: move to about page
     */
    @objc func moveToAbout() {
        let aboutCT = AboutViewController()
        present(aboutCT, animated: true, completion: nil)
    }
    
    /**
     * Function name: moveToReset
     * Function description: move to find password page
     */
    @objc func moveToReset() {
        let resetCT = ResetController()
        present(resetCT, animated: true, completion: nil)
    }
    
    /**
     * Function name: showWalkController
     * Function description: show the walk through controller at the very beginning
     */
    @objc func showWalkController() {
        let walkController = WalkController()
        present(walkController, animated: true, completion: {})
    }
    
    /**
     * Function name: changeButtonColor
     * Function description: change the button color when the input field is full filled
     */
    @objc private func changeButtonColor() {
        let isFieldFullFilled = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count ?? 0 > 0 && pwdTF.text?.characters.count ?? 0 > 0
        
        if isFieldFullFilled {
            loginButton.isEnabled = true
            loginButton.backgroundColor = UIColor.rgb(red: 166, green: 77, blue: 255)
        } else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        }
        
    }
    
    /**
     * Function name: handleLogin
     * Function description: perform login function when the user hit login button
     */
    @objc private func handleLogin() {
        SVProgressHUD.show(withStatus: "I'm working on it :)")
        UIApplication.shared.beginIgnoringInteractionEvents()
        guard let email = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        guard let password = pwdTF.text else {
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        Firebase.Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let ex = error {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Your email or password is wrong")
                SVProgressHUD.dismiss(withDelay: 1)
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            if (user?.user.isEmailVerified)! {
                SVProgressHUD.dismiss()
                SVProgressHUD.showSuccess(withStatus: "Login Success")
                SVProgressHUD.dismiss(withDelay: 1)
                guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                mainTabBarController.initView()
                self.dismiss(animated: true, completion: nil)
            } else {
                Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                    if let ex = error {
                        print("Verification Failed: \(ex)")
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                })
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Please verify your email address")
                SVProgressHUD.dismiss(withDelay: 1)
                UIApplication.shared.endIgnoringInteractionEvents()
//                let alert = UIAlertController(title: "Error for sign in", message: "Please verify your email address", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                self.present(alert,animated: true, completion: nil)
            }
            
        }
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    private func initialInputFields() {
        
        
        let stackView = UIStackView(arrangedSubviews: [emailTF, pwdTF, loginButton, registerButton, findPasswordButton])
        
        
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        _ = stackView.anchor(logoContainer.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 240)
        
        
        
    }
    
}
