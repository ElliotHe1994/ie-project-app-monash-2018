//
//  RegistrationController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 9/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import SVProgressHUD

/// Registration controller class for providing users to register to the system
class RegistrationController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var usernameList: [String]?
    
    let uploadProfileButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "uploadProfile").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleUploadProfileImage), for: .touchUpInside)
        return button
    }()
    
    let aboutButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("About", for: .normal)
        button.addTarget(self, action: #selector(moveToAbout), for: .touchUpInside)
        return button
    }()
    
    /**
     * Function name: handleUploadProfileImage
     * Function description: show up the image picker controller when user click upload profile images
     */
    @objc private func handleUploadProfileImage() {
        
        let imagePC = UIImagePickerController()
        imagePC.delegate = self
        imagePC.allowsEditing = true
        present(imagePC, animated: true, completion: nil)
        
    }
    
    /// perform actions after select the picture from image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let ediImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            uploadProfileButton.setImage(ediImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let oriImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            uploadProfileButton.setImage(oriImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        uploadProfileButton.layer.cornerRadius = uploadProfileButton.frame.width / 2
        uploadProfileButton.layer.masksToBounds = true
        uploadProfileButton.layer.borderColor = UIColor.purple.cgColor
        uploadProfileButton.layer.borderWidth = 3
        
        dismiss(animated: true, completion: nil)
        
    }
    
    let emailTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "email address"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let userTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "user name"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let pwdTF: UITextField = {
        let textField = UITextField()
        textField.placeholder = "password"
        textField.backgroundColor = UIColor(white:0, alpha: 0.03)
        textField.borderStyle = .roundedRect
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.isSecureTextEntry = true
        textField.addTarget(self, action: #selector(changeButtonColor), for: .editingChanged)
        return textField
    }()
    
    let registrationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Register", for: .normal)
        button.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleRegister), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Already have an account?", for: .normal)
        button.addTarget(self, action: #selector(moveToLogin), for: .touchUpInside)
        return button
    }()
    
    /**
     * Function name: moveToLogin
     * Function description: move back to login page when the user click "already have an account?"
     */
    @objc private func moveToLogin() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    /**
     * Function name: moveToAbout
     * Function description: move to about page when the user click about button
     */
    @objc func moveToAbout() {
        let aboutCT = AboutViewController()
        present(aboutCT, animated: true, completion: nil)
    }
    
    /**
     * Function name: checkUsername
     * Function description: check user name whether it exists
     */
    private func checkUsername() {
        Database.database().reference().child("usernames").observe(.value) { (snapshot) in
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let key = snap.key
                self.usernameList?.append(key)
                print(self.usernameList)
            }
        }
    }
    
    /**
     * Function name: handleRegister
     * Function description: perform register function after user click register button
     */
    @objc private func handleRegister() {
        SVProgressHUD.show(withStatus: "I'm working on it :)")
        guard let email = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines), email.characters.count > 0 else {return}
        guard let username = userTF.text?.trimmingCharacters(in: .whitespacesAndNewlines), username.characters.count > 0 else {return}
        guard let password = pwdTF.text, password.characters.count > 0 else {return}
        
        for name in usernameList! {
            if name == username {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Register Failed! The username exists.")
                SVProgressHUD.dismiss(withDelay: 1)
                return
            }
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let ex = error {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: "Register Failed! The email address exists.")
                SVProgressHUD.dismiss(withDelay: 1)
                return
            }
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                if let ex = error {
                    print("Verification Failed: \(ex)")
                    return
                }
            })
            
            print("Successfully registered user: \(email)")
            
            guard let image = self.uploadProfileButton.imageView?.image else {return}
            guard let uploadProfileImage = UIImageJPEGRepresentation(image, 0.4) else {return}
            
            let imageTitle = NSUUID().uuidString
            guard let id = user?.user.uid else {return}
            
            Database.database().reference().child("usernames").child(username).setValue(id, withCompletionBlock: { (error, databaseRef) in
                if let ex = error {
                    print("Failed to register user into the database: \(ex)")
                    return
                }
                print("Success add username.")
            })
            
            Storage.storage().reference().child("User_Profile_Images").child(imageTitle).putData(uploadProfileImage, metadata: nil, completion: { (url, error) in
                if let ex = error {
                    print("Failed to upload profile image: \(ex)")
                    return
                }
                print("Successfully upload profile image")
                
                Storage.storage().reference().child("User_Profile_Images").child(imageTitle).downloadURL(completion: { (url, error) in
                    if let ex = error {
                        print("Failed to grab the image URL \(ex)")
                        return
                    }
                    print("Success grab URL")
                    guard let profileImageURL = url?.absoluteString else {return}
                    guard let fcmToken = Messaging.messaging().fcmToken else {return}
                    
                    let values = ["username": username, "profileImage": profileImageURL, "fcmToken": fcmToken]
                    let userInfo = [id: values]
                    
                    Database.database().reference().child("users").updateChildValues(userInfo, withCompletionBlock: { (error, databaseRef) in
                        if let ex = error {
                            print("Failed to register user into the database: \(ex)")
                            return
                        }
                        
                        print("Successfully saved user into database")
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showSuccess(withStatus: "Register success! Please check your email to verify account before sign in")
                        SVProgressHUD.dismiss(withDelay: 2.5)
                        self.moveToLogin()
                    })
                })
            })
        }
        
    }
    
    /**
     * Function name: changeButtonColor
     * Function description: change button color when the user full filled all input fields
     */
    @objc private func changeButtonColor() {
        let isFieldFullFilled = emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count ?? 0 > 0 && userTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count ?? 0 > 0 && pwdTF.text?.characters.count ?? 0 > 0
        
        if isFieldFullFilled {
            registrationButton.isEnabled = true
            registrationButton.backgroundColor = UIColor.rgb(red: 166, green: 77, blue: 255)
        } else {
            registrationButton.isEnabled = false
            registrationButton.backgroundColor = UIColor.rgb(red: 242, green: 230, blue: 255)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameList = []
        self.view.backgroundColor = .white
        DispatchQueue.main.async {
            self.checkUsername()
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        view.addSubview(uploadProfileButton)
        view.addSubview(emailTF)
        view.addSubview(pwdTF)
        
        _ = uploadProfileButton.anchor(view.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 140, heightConstant: 140)
        uploadProfileButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        initialInputFields()
        
        view.addSubview(aboutButton)
        _ = aboutButton.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func initialInputFields() {
        
        
        let stackView = UIStackView(arrangedSubviews: [emailTF, userTF, pwdTF, registrationButton, loginButton])
        
        
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 10
        
        view.addSubview(stackView)
        
        _ = stackView.anchor(uploadProfileButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 240)
        
        
        
    }
    
    
    
}
