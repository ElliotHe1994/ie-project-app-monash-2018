//
//  Coordinate.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 23/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation

struct Coordinate {
    
    let longtitude: Double?
    let latitude: Double?
    let name: String?
    
    init(name: String, longtitude: Double, latitude: Double) {
        self.name = name
        self.longtitude = longtitude
        self.latitude = latitude
    }
}
