//
//  User.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 9/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation

struct User {
    
    let username: String
    let profileImage: String
    
    init(userInfo: [String: Any]) {
        self.username = userInfo["username"] as? String ?? ""
        self.profileImage = userInfo["profileImage"] as? String ?? ""
    }
    
}
