//
//  Post.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 23/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation

struct Post {
    let imageUrl: String
    let wildlifeName: String
    
    init(dictionary: [String: Any]) {
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        self.wildlifeName = dictionary["wildlifeName"] as? String ?? ""
    }
}
