//
//  Species.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 8/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation
import UIKit

class Species {
    let name: String
    let region: String
    let image: UIImage
    let density: String
    
    init(name: String, region: String, image: UIImage, density: String) {
        self.name = name
        self.region = region
        self.image = image
        self.density = density
    }
}
