//
//  Page.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 4/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation

struct Page {
    let title: String
    let imageName: String
    let message: String
}
