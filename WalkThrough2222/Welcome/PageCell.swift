//
//  PageCell.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 4/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import Foundation
import UIKit

/// custom collection view cell
class PageCell: UICollectionViewCell {
    
    
    var page: Page? {
        didSet {
            guard let page = page else {
                return
            }
            
            imageView.image = UIImage(named: page.imageName)
            
            let titleColor = UIColor(white: 0.2, alpha: 1)
            let titleFont = UIFont.systemFont(ofSize: 25, weight: .medium)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            let titleAttributes: [NSAttributedStringKey: Any] = [
                .font: titleFont,
                .foregroundColor: titleColor,
                .paragraphStyle: paragraphStyle
            ]
            let messageFont = UIFont.systemFont(ofSize: 20)
            let messageAttributes: [NSAttributedStringKey: Any] = [
                .font: messageFont,
                .foregroundColor: titleColor,
                .paragraphStyle: paragraphStyle
            ]
            let attributedText = NSMutableAttributedString(string: page.title, attributes: titleAttributes)
            attributedText.append(NSAttributedString(string: "\n\n\(page.message)", attributes: messageAttributes))
            
            
            
            
//            attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(10, 10))
            
            textView.attributedText = attributedText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .yellow
        iv.image = UIImage(named: "page1")
        iv.clipsToBounds = true
        return iv
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = ""
        tv.isEditable = false
        tv.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return tv
    }()
    
    let lineSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        backgroundColor = .white
        
        addSubview(imageView)
        addSubview(textView)
        addSubview(lineSeparatorView)
        imageView.anchorToTop(topAnchor, left: leftAnchor, bottom: textView.topAnchor, right: rightAnchor)
        
        textView.anchorWithConstantsToTop(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        textView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
        
        lineSeparatorView.anchorToTop(nil, left: leftAnchor, bottom: textView.topAnchor, right: rightAnchor)
        lineSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    
}
