//
//  AboutViewController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 6/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

/// About view controller displays the description of the project team
class AboutViewController: UIViewController {
    
    let logoView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "goback"), for: .normal)
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()
    
    @objc private func handleBack() {
        dismiss(animated: true, completion: nil)
    }
    
    let descriptLabel = UILabel()
    
    override func viewDidLoad() {
        descriptLabel.text = "AlphaTech\n\nA team with infinite possibilities"
        descriptLabel.numberOfLines = 0
        descriptLabel.font = UIFont.boldSystemFont(ofSize: 18)
        descriptLabel.textAlignment = .center
        view.backgroundColor = .white
        view.addSubview(logoView)
        view.addSubview(backButton)
        view.addSubview(descriptLabel)
        
        _ = backButton.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        _ = logoView.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 242, heightConstant: 200)
        logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        _ = descriptLabel.anchor(logoView.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 60, bottomConstant: 0, rightConstant: 60, widthConstant: 0, heightConstant: 0)
        
        super.viewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
