//
//  ForumController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 10/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase

/// Website controller shows the website
class WebsiteController: UIViewController, UIWebViewDelegate {
    
    let myWebView: UIWebView = UIWebView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationController?.navigationBar.isHidden = true
        navigationItem.title = "Wildlife"
        
        view.addSubview(myWebView)
        _ = myWebView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        myWebView.delegate = self
        let myUrl = NSURL(string: "https://dr-wildlife.tk")
        let myUrlRequest = NSURLRequest(url:myUrl! as URL)
        myWebView.loadRequest(myUrlRequest as URLRequest)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
