//
//  CommunityMapViewController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 1/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import FirebaseDatabase
import GoogleMaps

/// Community map controller shows all users posts on the map
class CommunityMapViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        initLocationManager()
        initGoogleMap()
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.indoorPicker = true
        navigationItem.title = "Map"
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "refresh"), style: .plain, target: self, action: #selector(fetchCurrentLocation))
    }
    
//    @objc private func fetchCurrentLocation() {
//        locationManager.startUpdatingLocation()
//    }
    
    
    private func initGoogleMap() {
        self.mapView = GMSMapView()
        mapView.frame = CGRect.zero
        view = mapView
        let ref = Database.database().reference().child("Wildlife_Posts")
        DispatchQueue.main.async {
            ref.observe(.childAdded) { (snapshot) in
                ref.observe(DataEventType.value, with: { (snapshot) in
                    if snapshot.value != nil {
                        self.mapView.clear()
                        for child in snapshot.children {
                            let snap = child as! DataSnapshot
                            for item in snap.children.allObjects as! [DataSnapshot] {
                                guard let Dict = item.value as? [String: AnyObject] else {
                                    continue
                                }
                                let latitude = Dict["latitude"]
                                let longtitude = Dict["longtitude"]
                                let wildlifeName = Dict["wildlifeName"]
                                let wildlifeImage = Dict["imageUrl"]
                                
                                
                                let iv = CustomIconView(frame: CGRect.init(x: 0, y: 0, width: 80, height: 80))
                                iv.loadImage(url: wildlifeImage as! String)
                                let marker = GMSMarker()
                                
                                let postUserId = snap.key
                                var username = "username"
                                let refUser = Database.database().reference().child("users").child(postUserId)
                                refUser.observeSingleEvent(of: .value, with: { (snapshot) in
                                    let value = snapshot.value as? NSDictionary
                                    username = value?["username"] as! String
                                    marker.snippet = username
                                }, withCancel: { (error) in
                                    print("Retrieve username error: \(error)")
                                })
                                
                                marker.position = CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees, longitude: longtitude as! CLLocationDegrees)
                                marker.title = wildlifeName as? String
                                marker.iconView = iv
                                marker.map = self.mapView
                                self.mapView.selectedMarker = marker
                            }
                        }
                    }
                })
            }
        }
    }
    
    private func initLocationManager() {
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)
            self.mapView.animate(to: camera)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}

