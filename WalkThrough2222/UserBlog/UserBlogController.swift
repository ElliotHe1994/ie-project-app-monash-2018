//
//  UserBlogController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 9/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import MaterialComponents.MaterialCards

protocol UserBlogHeaderDelegate {
    func didShowListView()
    func didShowGridView()
    func didChangeProfilePicture()
}

/// User blog controller provides the profile page for users
class UserBlogController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UserBlogHeaderDelegate {
    
    func didShowListView() {
        isGridView = false
        collectionView?.reloadData()
    }
    
    func didShowGridView() {
        isGridView = true
        collectionView?.reloadData()
    }
    
    func didChangeProfilePicture() {
        isGridView = true
    }
    
    var isGridView = true
    var currentUser: User?
    let cellId = "cellId"
    let listViewCellId = "listViewCellId"
    var posts = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        getCurrentName()
        collectionView?.register(UserBlogHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        collectionView?.register(UserBlogCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(ListViewCell.self, forCellWithReuseIdentifier: listViewCellId)
    
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        initLogOutButton()
        fetchOrderedWildlife()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func handleRefresh() {
        self.posts.removeAll()
        fetchOrderedWildlife()
        getCurrentName()
    }
    
    /**
     * Function name: fetchOrderedWildlife
     * Function description: fetch the own posts from firebase database in the descending order of the timestamp
     */
    private func fetchOrderedWildlife() {
        DispatchQueue.global(qos: .background).async {
            guard let uid = Auth.auth().currentUser?.uid else {return}
            let ref = Database.database().reference().child("Wildlife_Posts").child(uid)
            
            ref.queryOrdered(byChild: "witnessTime").observe(.childAdded, with: { (snapshot) in
                print(snapshot.key, snapshot.value)
                self.collectionView?.refreshControl?.endRefreshing()
                guard let myDiary = snapshot.value as? [String:Any] else {
                    return
                }
                let post = Post(dictionary: myDiary)
//                self.posts.append(post)
                self.posts.insert(post, at:0)
                self.collectionView?.reloadData()
            }) { (error) in
                print("Encountered error \(error)")
            }
        }
    }
    
    private func initLogOutButton() {
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "logout").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(moreAction))
        
    }
    
    @objc func moreAction() {
        
        let alertCT = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertCT.addAction(UIAlertAction(title: "Log out", style: .destructive, handler: { (action) in
            do {
                try Firebase.Auth.auth().signOut()
                let loginController = LoginController()
                let navController = UINavigationController(rootViewController: loginController)
                self.present(navController, animated: true, completion: nil)
            } catch let ex {
                print("Failed sign out: \(ex)")
            }
        }))
        alertCT.addAction(UIAlertAction(title: "Change password", style: .default, handler: { (action) in
            let resetController = ResetController()
            self.present(resetController, animated: true, completion: nil)
        }))
        alertCT.addAction(UIAlertAction(title: "About", style: .default, handler: { (action) in
            let aboutController = AboutViewController()
            self.present(aboutController, animated: true, completion: nil)
        }))
        alertCT.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertCT.popoverPresentationController?.sourceView = view
        present(alertCT, animated: true, completion: nil)
    }
    
    /**
     * Function name: getCurrentName
     * Function description: fetch the current user's username and profile image url
     */
    private func getCurrentName() {

        guard let userId = Firebase.Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("users").child(userId).observeSingleEvent(of: .value, with: { (snapShot) in
            print(snapShot.value ?? "")
            guard let userInfo = snapShot.value as? [String: Any] else {return}
            
            self.currentUser = User(userInfo: userInfo)
            self.navigationItem.title = self.currentUser?.username
            self.collectionView?.reloadData()
        }) { (error) in
            print("Failed to get currect user: \(error)")
        }
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! UserBlogHeader
        
        header.user = self.currentUser
        header.delegate = self

        return header
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 209)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isGridView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! UserBlogCell
            cell.post = posts[indexPath.item]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listViewCellId, for: indexPath) as! ListViewCell
            cell.post = posts[indexPath.item]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isGridView {
            let length = (view.frame.width - 3) / 4
            return CGSize(width: length, height: length)
        } else {
            var height: CGFloat = view.frame.width
            height += 40
            return CGSize(width: view.frame.width, height: height)
        }
//        let length = view.frame.width - 20
//        let height = CGFloat(200)
//        return CGSize(width: length, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
//        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
//        return 15
    }
    
}
