//
//  ListViewCell.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 26/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

/// List view controller provides the list view of the users' posts
class ListViewCell: UICollectionViewCell {
    
    var post: Post? {
        didSet {
            guard let imageUrl = post?.imageUrl else {return}
            wildlifeImageView.loadImage(url: imageUrl)
            guard let imageName = post?.wildlifeName else {return}
            wildlifeNameLabel.text = imageName
        }
    }
    
    let wildlifeImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let wildlifeNameLabel: UILabel = {
        let label = UILabel()
        label.text = ""
//        let attributedText = NSMutableAttributedString(string: "Username")
//        label.attributedText = attribute
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(wildlifeNameLabel)
        wildlifeNameLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 40)
        
        addSubview(wildlifeImageView)
        wildlifeImageView.anchor(wildlifeNameLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        wildlifeImageView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
