//
//  UserBlogCell.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 23/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards

/// custom collection view cell
class UserBlogCell: MDCCardCollectionCell {
    
    var post: Post? {
        didSet {
            guard let imageUrl = post?.imageUrl else {return}
            cellImageView.loadImage(url: imageUrl)
            guard let imageName = post?.wildlifeName else {return}
            textView.text = imageName
        }
    }
    
    let cardView = MDCCard()
    
    let cellImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "sdfsfsf"
        tv.isEditable = false
        return tv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        addSubview(cardView)
//        _ = cardView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//
//        cardView.addSubview(cellImageView)
//        _ = cellImageView.anchor(cardView.topAnchor, left: cardView.leftAnchor, bottom: cardView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 0)
//        cardView.addSubview(textView)
//        _ = textView.anchor(cardView.topAnchor, left: cellImageView.rightAnchor, bottom: cardView.bottomAnchor, right: cardView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        addSubview(cellImageView)
        _ = cellImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
