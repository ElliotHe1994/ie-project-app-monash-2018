//
//  UserBlogHeader.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 9/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit

/// custom collection view header
class UserBlogHeader: UICollectionViewCell {
    
    var delegate: UserBlogHeaderDelegate?
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.backgroundColor = .white
        return imageView
    }()
    
    let editProfileButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Change profile picture", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.borderColor = UIColor.rgb(red: 204, green: 153, blue: 255).cgColor
        button.addTarget(self, action: #selector(handleProfileChange), for: .touchUpInside)
        button.layer.borderWidth = 1
        return button
    }()
    
    lazy var gridViewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
        button.addTarget(self, action: #selector(handleGridView), for: .touchUpInside)
        return button
    }()
    
    lazy var listViewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.tintColor = UIColor(white: 0, alpha: 0.2)
        button.addTarget(self, action: #selector(handleListView), for: .touchUpInside)
        return button
    }()
    
    @objc func handleGridView() {
        gridViewButton.tintColor = .chooseBlue()
        listViewButton.tintColor = UIColor(white: 0, alpha: 0.2)
        delegate?.didShowGridView()
    }
    
    @objc func handleListView() {
        gridViewButton.tintColor = UIColor(white: 0, alpha: 0.2)
        listViewButton.tintColor = .chooseBlue()
        delegate?.didShowListView()
    }
    
    @objc private func handleProfileChange() {
        
        delegate?.didChangeProfilePicture()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        addSubview(profileImageView)
//        addSubview(editProfileButton)
        _ = profileImageView.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 130, heightConstant: 130)
        profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        profileImageView.layer.cornerRadius = 130 / 2
        profileImageView.clipsToBounds = true
        
//        _ = editProfileButton.anchor(profileImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 200, heightConstant: 35)
        
        initToolBar()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init error")
    }
    
    var user: User? {
        didSet {
            guard let profileImageUrl = user?.profileImage else {return}
            profileImageView.loadImage(url: profileImageUrl)
        }
    }
    
    private func initToolBar() {
        
        let stackView = UIStackView(arrangedSubviews: [gridViewButton, listViewButton])
        addSubview(stackView)
        _ = stackView.anchor(profileImageView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        
        let topView = UIView()
        topView.backgroundColor = UIColor.rgb(red: 217, green: 179, blue: 255)
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.rgb(red: 217, green: 179, blue: 255)
        
        addSubview(topView)
        addSubview(bottomView)
        
        topView.anchor(stackView.topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        bottomView.anchor(stackView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
    }
    
    
}
