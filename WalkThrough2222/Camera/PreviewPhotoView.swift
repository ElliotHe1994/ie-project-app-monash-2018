//
//  PreviewPhotoView.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 1/9/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Photos

/// Preview photo shows the image that token by the users
class PreviewPhotoView: UIView {
    
    var coordinate: CLLocation?
    
    let previewImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "goback"), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
    }()
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "save"), for: .normal)
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        return button
    }()
    
    @objc private func handleCancel() {
        self.removeFromSuperview()
    }
    
    /**
     * Function name: handleSave
     * Function description: save the picture into the local photo library
     */
    @objc private func handleSave() {
        
        guard let image = previewImageView.image else {return}
        let library = PHPhotoLibrary.shared()
        library.performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: image)
        }) { (success, ex) in
            if let error = ex {
                print("Cannot save the photo: \(error)")
                return
            }
            DispatchQueue.main.async {
                let hintLabel = UILabel()
                hintLabel.text = "Photo Saved"
                hintLabel.textColor = .white
                hintLabel.font = UIFont.boldSystemFont(ofSize: 18)
                hintLabel.textAlignment = .center
                hintLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
                hintLabel.numberOfLines = 0
                hintLabel.frame = CGRect(x: 0, y: 0, width: 130, height: 80)
                hintLabel.center = self.center
                self.addSubview(hintLabel)
            
                hintLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    hintLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
                }, completion: { (_) in
                    UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                        hintLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                        hintLabel.alpha = 0
                    }, completion: { (_) in
                        hintLabel.removeFromSuperview()
                    })
                })
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    private func initView() {
        addSubview(previewImageView)
        _ = previewImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        addSubview(closeButton)
        _ = closeButton.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        addSubview(saveButton)
        _ = saveButton.anchor(nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 24, rightConstant: 0, widthConstant: 80, heightConstant: 80)
        saveButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
