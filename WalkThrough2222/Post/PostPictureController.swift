//
//  PostPictureController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 23/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

/// Post controller provides functions for post the wildlife images
class PostPictureController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var pickerView = UIPickerView()
    
    var animalArray = ["Azure kingfisher","Black kite","Black-eared cuckoo","Black-faced cormorant","Black-faced cuckooshrike","Black-fronted dotterel","Black-winged stilt","Blue whale","Brown fur seal","Brush-tailed rock-wallaby","Bryde's whale","Common wallaroo","Eastern barred bandicoot","Eastern grey kangaroo","Humpback whale","Koala","Leadbeater's possum","Little Wattlebird","Long-footed potoroo","Mountain pygmy possum","Painted burrowing frog","Pigeon","Platypus","Red kangaroo","Red wattlebird","Red-browed finch","Red-necked wallaby","Red-tailed Black-Cockatoo","Rufous rat-kangaroo","Rufous whistler","Sacred kingfisher","Sand goanna","Sharp-tailed sandpiper","Short-beaked echidna","Southern hairy-nosed wombat","Tiger quoll","Western grey kangaroo","Western pygmy possum","Yellow-tailed black cockatoo"]
    
    var selectedPicture: UIImage? {
        didSet {
            self.pictureView.image = selectedPicture
        }
    }
    
    var longitude: Double = 0.00
    var latitude: Double = 0.00
    
    let pictureView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .red
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    
    
    let textView: UITextField = {
        let text = UITextField()
        text.font = UIFont.systemFont(ofSize: 14)
        text.placeholder = "Please select an animal"
        return text
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(postAction))
        
        initPost()
        initPickerView()
        print("long: \(longitude)+ lat: \(latitude)")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func initPickerView() {
        pickerView.delegate = self
        pickerView.dataSource = self
        textView.inputView = pickerView
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return animalArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return animalArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textView.text = animalArray[row]
        self.view.endEditing(false)
    }
    
    private func initPost() {
        let containerView = UIView()
        containerView.backgroundColor = .white
        
        view.addSubview(containerView)
        _ = containerView.anchor(topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
        
        containerView.addSubview(pictureView)
        containerView.addSubview(textView)
        _ = pictureView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 84, heightConstant: 0)
        
        _ = textView.anchor(containerView.bottomAnchor, left: pictureView.rightAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 84)
        
    }
    
    /**
     * Function name: postAction
     * Function description: post the image and the wildlife name to the firebase database
     */
    @objc private func postAction() {
        guard let image = selectedPicture else {return}
        guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else {return}
        guard let wildlifeName = textView.text, wildlifeName.characters.count > 0 else {return}
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        SVProgressHUD.show(withStatus: "I'm working on it :)")
        let imageTitle = NSUUID().uuidString
        Storage.storage().reference().child("Wildlife_Posts").child(imageTitle).putData(uploadData, metadata: nil) { (metadata, error) in
            if let ex = error {
                SVProgressHUD.dismiss()
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                let alert = UIAlertController(title: "Failed post", message: "There're some issues for uploading your picture", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert,animated: true, completion: nil)
                return
            }
            Storage.storage().reference().child("Wildlife_Posts").child(imageTitle).downloadURL(completion: { (url, error) in
                if let ex = error {
                    print("Failed to grab the image URL \(ex)")
                    return
                }
                guard let imageUrl = url?.absoluteString else {return}
                print("Successful upload images: \(imageUrl)")
                
                self.saveToDatabase(imageUrl: imageUrl)
            })
            
        }
    }
    
    private func saveToDatabase(imageUrl: String) {
        guard let postPicture = selectedPicture else {return}
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let post = Database.database().reference().child("Wildlife_Posts").child(uid)
        let ref = post.childByAutoId()
        let values = ["wildlifeName": textView.text, "witnessTime": Date().timeIntervalSince1970, "longtitude": longitude, "latitude": latitude, "imageUrl": imageUrl, "imageWidth": postPicture.size.width, "imageHeight": postPicture.size.height] as [String : Any]
        ref.updateChildValues(values) { (error, ref) in
            if let ex = error {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("Failed to save to the database")
                return
            }
            print("Successfully save to database.")
            SVProgressHUD.dismiss()
            self.dismiss(animated: true, completion: nil)
        }
    }
}
