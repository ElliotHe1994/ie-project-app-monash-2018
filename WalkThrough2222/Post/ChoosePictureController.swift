//
//  ChoosePictureController.swift
//  WalkThrough2222
//
//  Created by ElliotMo on 10/8/18.
//  Copyright © 2018 Elliot He. All rights reserved.
//

import UIKit
import Photos
import Firebase
import SVProgressHUD

class ChoosePictureController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate {
    
    let cellId = "cellId"
    let headerId = "headerId"
    var images = [UIImage]()
    var selectedPicture: UIImage?
    var assets = [PHAsset]()
    var header: PictureHeader?
    var coordinate: CLLocation?
    var currentLocation: CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var result = ["Not wildlife"]
    var approve = false
    
    let newPostButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "addNew"), for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    lazy var vision = Vision.vision()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        initialTopButtons()
        collectionView?.register(PictureCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(PictureHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        
        view.addSubview(newPostButton)
        _ = newPostButton.anchor(nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 13, rightConstant: 13, widthConstant: 100, heightConstant: 100)
        
        fetchPictures()
        initLocationManager()
    }
    
    /**
     * Function name: detectLabels
     * Function description: identify the pictures the user has chosen preparing for post
     */
    @objc private func detectLabels() {
        guard let image = header?.pictureImageView.image else {return}
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show(withStatus: "I'm working on it :)")
        result = ["Not wildlife"]
        approve = false
        let options = VisionCloudDetectorOptions()
        options.modelType = .latest
        options.maxResults = 20
        
        let labelDetector = vision.cloudLabelDetector()
        let visionImage = VisionImage(image: image)
        labelDetector.detect(in: visionImage) { (features, error) in
            guard error == nil, let features = features, !features.isEmpty else {
                let errorString = error?.localizedDescription ?? "No results returned."
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            self.result = features.map { feature -> String in
                return feature.label ?? "No result"
            }
            if self.result.contains("wildlife") || self.result.contains("bird") || self.result.contains("mammal") || self.result.contains("animal") {
                self.approve = true
                SVProgressHUD.showSuccess(withStatus: "I guess it is \(self.result[0]) :)")
                SVProgressHUD.dismiss(withDelay: 2)
            } else {
                self.approve = false
                SVProgressHUD.showError(withStatus: "Sorry, I believe this is \(self.result[0]) which is not allowed to be posted :(")
                SVProgressHUD.dismiss(withDelay: 2)
            }
            self.changeNavigationBarItems()
            UIApplication.shared.endIgnoringInteractionEvents()
            print("Result: \(self.result)")
        }
    }
    
    /**
     * Function name: changeNavigationBarItems
     * Function description: change the status of the picture
     */
    private func changeNavigationBarItems() {
        let notApprove = UIImageView(image: #imageLiteral(resourceName: "PleaseCheck"))
//        notApprove.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        let isApprove = UIImageView(image: #imageLiteral(resourceName: "Identified"))
//        isApprove.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        if approve {
            navigationItem.titleView = isApprove
            newPostButton.isEnabled = true
        } else {
            navigationItem.titleView = notApprove
            newPostButton.isEnabled = false
        }
    }
    
    /**
     * Function name: initLocationManager
     * Function description: initialize the location manager in order to get current location
     */
    private func initLocationManager() {
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.currentLocation = location.coordinate
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedPicture = images[indexPath.item]
        self.approve = false
        changeNavigationBarItems()
        self.collectionView?.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
    }
    
    private func fetchOptions() -> PHFetchOptions {
        let fetchOption = PHFetchOptions()
        fetchOption.fetchLimit = 2000
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchOption.sortDescriptors = [sortDescriptor]
        return fetchOption
    }
    
    /**
     * Function name: fetchPictures
     * Function description: fetch pictures from the photo library
     */
    private func fetchPictures() {
        
        let pictures = PHAsset.fetchAssets(with: .image, options: fetchOptions())
        
        DispatchQueue.global(qos: .background).async {
            pictures.enumerateObjects { (asset, count, stop) in
                let imageManager = PHImageManager.default()
                let targetSize = CGSize(width: 60, height: 60)
                let options = PHImageRequestOptions()
                options.isSynchronous = true
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                    if let image = image {
                        self.images.append(image)
                        self.assets.append(asset)
                        if self.selectedPicture == nil {
                            self.selectedPicture = image
                        }
                    }
                    if count == pictures.count - 1 {
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                        }
                    }
                })
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PictureCell
        cell.pictureImageView.image = images[indexPath.item]
        return cell
    }
    
    private func initialTopButtons() {
        
        navigationController?.navigationBar.tintColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Check", style: .plain, target: self, action: #selector(detectLabels))
        changeNavigationBarItems()
        
    }
    
    @objc private func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    /**
     * Function name: nextAction
     * Function description: present the post page after identifying the picture
     */
    @objc private func nextAction() {
        let postController = PostPictureController()
        postController.selectedPicture = header?.pictureImageView.image
        guard let coordinate = self.coordinate else {
            guard let currentcoordinate = self.currentLocation else {return}
            postController.longitude = currentcoordinate.longitude
            postController.latitude = currentcoordinate.latitude
            navigationController?.pushViewController(postController, animated: true)
            return
//            let alert1 = UIAlertController(title: "Failed post", message: "Your picture doesn't contain the valid coordinates. Please choose a wildlife pictures with coordinates.", preferredStyle: .alert)
//            alert1.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            self.present(alert1,animated: true, completion: nil)
//            return
        }
        postController.longitude = coordinate.coordinate.longitude
        postController.latitude = coordinate.coordinate.latitude
        navigationController?.pushViewController(postController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width - 4) / 5
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = view.frame.width
        
        return CGSize(width: width, height: width)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! PictureHeader
        self.header = headCell
        headCell.pictureImageView.image = selectedPicture
        if let selectedPicture = selectedPicture {
            if let index = self.images.index(of: selectedPicture) {
                let selectedAsset = self.assets[index]
                self.coordinate = selectedAsset.location
                let imageManager = PHImageManager.default()
                let newSize = CGSize(width: 600, height: 600)
                imageManager.requestImage(for: selectedAsset, targetSize: newSize, contentMode: .default, options: nil) { (image, info) in
                    headCell.pictureImageView.image = image
                }
            }
        }
        return headCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
    }
    
    
}
